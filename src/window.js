module.exports = function(callback) {
  return (function() {
    callback(this);
  }).call(null);
};
