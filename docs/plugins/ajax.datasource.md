# ajax.datasource

ajax.datasource is a droplab plugin that allows for retrieving and rendering list data from a server.

It extends the `DropLab.prototype.addData` function.

## Install

* Add [`/dist/plugins/ajax.datasource.js`](/dist/plugins/ajax.datasource.js) to your application assets.
* Link to them in your code. See [examples](/README.md#examples).

## Usage

Use `addData` as described in the ["Dynamic data"](/README.md#dynamic-data) but instead of providing
an array, pass a path string as the second argument to be used as the request endpoint.

```html
<a href="#" data-dropdown-trigger="#dynamic-dropdown" data-id="dynamic-trigger">Toggle</a>

<ul id="dynamic-dropdown" data-dropdown data-dynamic>
  <li><a href="#" data-id="{{id}}">{{text}}</a></li>
</ul>
```

```js
droplab.addData('dynamic-trigger', '/data.json');
```

You are still able to pass an array, so you can use both.

```js
droplab.addData('dynamic-trigger', '/data.json');

droplab.addData('dynamic-trigger', [{
  id: 0,
  text: 'Jacob'
}, {
  id: 1,
  text: 'Jeff'
}]);
```
