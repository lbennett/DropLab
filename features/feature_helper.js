/* eslint-env node, mocha */
var browser = require('./support/browser');

var windowHelpers = {
  winEval: function(callback) {
    try {
      callback.call(this.window, this.window, this.document);
    } catch(e) {
      console.error(e.message, e.stack); // eslint-disable-line no-console
      throw e;
    }
  },

  $: function(selector, root) {
    return (root || this.document).querySelector(selector);
  },

  $$: function(selector, root) {
    return (root || this.document).querySelectorAll(selector);
  },

  trigger: function(elem, eventName, props) {
    var event = this.document.createEvent('Event');
    event.initEvent(eventName, true, true);
    Object.assign(event, props || {});
    elem.dispatchEvent(event);
  },
};

global.loadPage = function(path, src) {
  beforeEach(function() {
    Object.assign(this, windowHelpers);

    return browser.load(path, src).then(function(window) {
      this.window = window;
      this.document = window.document;
    }.bind(this));
  });

  afterEach(function() {
    this.window && this.window.close();
    delete this.window;
    delete this.document;
    Object.keys(windowHelpers).forEach(function(key) {
      delete this[key];
    }.bind(this));
  });
};

global.chai = require('chai');
global.expect = global.chai.expect;
global.sinon = require('sinon');
global.chai.use(require('sinon-chai'));
